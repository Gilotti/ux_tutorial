﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class PanelAddressEdit : MonoBehaviour {

	public GameObject MainManager;

	public InputField ZipCode;
	public InputField Street;
	public InputField Number;
	public InputField Complement;
	public InputField Neighborhood;
	public InputField City;
	public InputField State;
	public InputField Country;

	private WebServices _webServices;

	private AddressDTO PanelDTO;

	// Use this for initialization
	void Start () {
		Initialize ();
	}

	// Update is called once per frame
	void Update () {
	}


	//========================================================================
	private void Initialize(){

		_webServices = MainManager.GetComponent (typeof(WebServices)) as WebServices;

		if (_webServices != null) {
			PopulateFromDTO (PanelDTO);
		}
	}

	//========================================================================
	private void PopulateFromDTO (AddressDTO _dto)
	{
		if (!string.IsNullOrEmpty (_dto.ZipCode)) {
			ZipCode.text = _dto.ZipCode;
		}

		if (!string.IsNullOrEmpty (_dto.Route)) {
			Street.text = _dto.Route;
		}
	
		if (_dto.Number != null) {
			Number.text = _dto.Number.ToString();
		}

		if (!string.IsNullOrEmpty (_dto.Complement)) {
			Complement.text = _dto.Complement;
		}

		if (!string.IsNullOrEmpty (_dto.Neighborhood)) {
			Neighborhood.text = _dto.Neighborhood;
		}
	
		if (!string.IsNullOrEmpty (_dto.City)) {
			City.text = _dto.City;
		}

		if (!string.IsNullOrEmpty (_dto.State)) {
			State.text = _dto.State;
		}
	
		if (!string.IsNullOrEmpty (_dto.Country)) {
			Country.text = _dto.Country;
		}
	}

}
