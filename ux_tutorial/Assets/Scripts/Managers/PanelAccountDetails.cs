﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;


public class PanelAccountDetails : MonoBehaviour {

	public GameObject MainManager;

	public InputField UserName;
	public InputField FullName;
	public InputField Email;
	public Dropdown Language;
	public Dropdown TimeZone;
	public ToggleGroup GenderType;
	public ToggleGroup Avatar;

	private WebServices _webServices;
	private IList<string> _languages;
	private IList<string> _timeZones;

	private UserDTO PanelDTO;

	// Use this for initialization
	void Start () {
		Initialize ();
	}
	
	// Update is called once per frame
	void Update () {
	}


	//========================================================================
	private void Initialize(){

		_webServices = MainManager.GetComponent (typeof(WebServices)) as WebServices;

		if (_webServices != null) {
			PanelDTO = _webServices.GetAccountDetailsDTO ();
			PopulateFromDTO (PanelDTO);
		}
	}

	//========================================================================
	private void PopulateFromDTO (UserDTO _dto)
	{
		if (!string.IsNullOrEmpty (_dto.UserName)) {
			UserName.text = _dto.UserName;
		}

		if (!string.IsNullOrEmpty (_dto.Name)) {
			FullName.text = _dto.Name;
		}

		if (!string.IsNullOrEmpty (_dto.Email)) {
			Email.text = _dto.Email;
		}

		//=====Language is a Dropdown

		//=====Timezone is a Dropdown

		//=====GenderType is a ToggleGroup

		//=====Avatar is a ToggleGroup
	}
}
