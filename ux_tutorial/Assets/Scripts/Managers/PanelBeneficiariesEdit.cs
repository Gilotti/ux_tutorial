﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PanelBeneficiariesEdit : MonoBehaviour {

	public GameObject MainManager;

	public InputField Relationship;
	public InputField Name;
	public InputField DateOFBirth;

	private WebServices _webServices;

	private UserBeneficiariesDTO PanelDTO;

	// Use this for initialization
	void Start () {
		Initialize ();
	}

	// Update is called once per frame
	void Update () {
	}


	//========================================================================
	private void Initialize(){

		_webServices = MainManager.GetComponent (typeof(WebServices)) as WebServices;

		if (_webServices != null) {
			PopulateFromDTO (PanelDTO);
		}
	}

	//========================================================================
	private void PopulateFromDTO (UserBeneficiariesDTO _dto)
	{
		if (!string.IsNullOrEmpty (_dto.Relationship)) {
			Relationship.text = _dto.Relationship;
		}

		if (!string.IsNullOrEmpty (_dto.Name)) {
			Name.text = _dto.Name;
		}

		if (!string.IsNullOrEmpty (_dto.DateOfBirth)) {
			DateOFBirth.text = _dto.DateOfBirth;
		}
	
	}

}
