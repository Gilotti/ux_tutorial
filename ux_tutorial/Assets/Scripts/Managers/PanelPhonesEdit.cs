﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PanelPhonesEdit : MonoBehaviour {

	public GameObject MainManager;

	public InputField Type;
	public InputField CountryCode;
	public InputField CityCode;
	public InputField PhoneNumber;
	public InputField Complement;

	private WebServices _webServices;

	private PhoneDTO PanelDTO;

	// Use this for initialization
	void Start () {
		Initialize ();
	}

	// Update is called once per frame
	void Update () {
	}


	//========================================================================
	private void Initialize(){

		_webServices = MainManager.GetComponent (typeof(WebServices)) as WebServices;

		if (_webServices != null) {
			PopulateFromDTO (PanelDTO);
		}
	}

	//========================================================================
	private void PopulateFromDTO (PhoneDTO _dto)
	{
		if (!string.IsNullOrEmpty (_dto.Type)) {
			Type.text = _dto.Type;
		}

		if (_dto.CountryCode > 0) {
			CountryCode.text = _dto.CountryCode.ToString();
		}

		if (_dto.CityCode > 0) {
			CityCode.text = _dto.CityCode.ToString();
		}
			
		if (_dto.PhoneNumber > 0) {
			PhoneNumber.text = _dto.PhoneNumber.ToString();
		}

		if (_dto.Complement != null) {
			Complement.text = _dto.Complement.ToString();
		}
	}

}
