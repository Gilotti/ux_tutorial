﻿using UnityEngine;
using System.Collections;

public class OpeningAnimator : MonoBehaviour {

	public float AnimationTime = 1.0f;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnEnable (){
		Open ( gameObject, AnimationTime);
	}

	public void Open (GameObject _openingObject, float _animationTime){
		
		iTween.ScaleFrom (_openingObject, iTween.Hash ("scale", Vector3.zero,  "easeType", "easeOutElastic", "time", _animationTime ));
	}


	public void Close (GameObject _openingObject, float _animationTime){
		
		iTween.ScaleTo (_openingObject, iTween.Hash ("scale", Vector3.zero,  "easeType", "easeInBack", "time", _animationTime ));
	}
}
