﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class PanelDocsEdit : MonoBehaviour {

	public GameObject MainManager;

	public InputField UserDocumentType;
	public InputField DocumentNumber;
	public InputField ParentName;
	public InputField EmittedDate;
	public InputField Emitter;

	private WebServices _webServices;

	private BiaDocumentsDTO PanelDTO;

	// Use this for initialization
	void Start () {
		Initialize ();
	}

	// Update is called once per frame
	void Update () {
	}


	//========================================================================
	private void Initialize(){

		_webServices = MainManager.GetComponent (typeof(WebServices)) as WebServices;

		if (_webServices != null) {
			PopulateFromDTO (PanelDTO);
		}
	}

	//========================================================================
	private void PopulateFromDTO (BiaDocumentsDTO _dto)
	{
		if ( _dto.DucumentType != BiaTypesEnum.DocumentType.None) {
			UserDocumentType.text = _dto.ParentName.ToString();
		}
			
		if (!string.IsNullOrEmpty (_dto.DocumentNumber)) {
			DocumentNumber.text = _dto.DocumentNumber;
		}

		if (!string.IsNullOrEmpty (_dto.ParentName)) {
			ParentName.text = _dto.ParentName;
		}

		if (!string.IsNullOrEmpty (_dto.EmittedDate)) {
			EmittedDate.text = _dto.EmittedDate;
		}

		if (!string.IsNullOrEmpty (_dto.Emitter)) {
			Emitter.text = _dto.Emitter;
		}

	}
}