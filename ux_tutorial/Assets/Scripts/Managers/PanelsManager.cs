﻿using UnityEngine;
using System.Collections;

public class PanelsManager : MonoBehaviour {

	public GameObject Footer;
	public GameObject PanelLogin;
	public GameObject PanelCards;
	public GameObject PanelProducts;
	public GameObject PanelStatement;
	public GameObject PanelLuckyDraw;



	//=========================================================
	// Use this for initialization
	void Start () {
		OpenPanel (PanelCards);
		OpenPanel (Footer);
	}
	
	// Update is called once per frame
	void Update () {
	}


	//=========================================================
	private void OpenPanel ( GameObject PanelToOpen)
	{
		PanelToOpen.SetActive (true);
	}

	//=========================================================
	private void ClosePanel ( GameObject PanelToClose )
	{
		PanelToClose.SetActive (false);
	}



	//=========================================================
	public void OpenPanel_PanelCard (){
		ClosePanel_All ();
		OpenPanel (PanelCards);
	}

	//=========================================================
	public void OpenPanel_PanelProduct (){
		ClosePanel_All ();
		OpenPanel (PanelProducts);
	}

	//=========================================================
	public void OpenPanel_PanelStatement (){
		ClosePanel_All ();
		OpenPanel (PanelStatement);
	}
		
	//=========================================================
	public void OpenPanel_PanelLuckyDraw (){
		ClosePanel_All ();
		OpenPanel (PanelLuckyDraw);
	}




	//=========================================================
	public void ClosePanel_PanelCard (){
		ClosePanel (PanelCards);
	}

	//=========================================================
	public void ClosePanel_PanelProduct (){
		ClosePanel (PanelProducts);
	}

	//=========================================================
	public void ClosePanel_PanelStatement (){
		ClosePanel (PanelStatement);
	}

	//=========================================================
	public void ClosePanel_PanelLuckyDraw (){
		ClosePanel (PanelLuckyDraw);
	}

	//=========================================================
	private void ClosePanel_All (){
		ClosePanel (PanelCards);
		ClosePanel (PanelProducts);
		ClosePanel (PanelStatement);
		ClosePanel (PanelLuckyDraw);
	}
}
