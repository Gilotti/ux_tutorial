﻿using UnityEngine;
using System.Collections;

public class DocsAddDTO : MonoBehaviour {

	public string UserDocumentType { get; set;}
	public string DocumentNumber { get; set;}
	public string EmittedDate { get; set;}
	public string ParentName { get; set;}
	public string Emitter { get; set;}
	public string DocParentType { get; set;}
	public string ParentId { get; set;}

}
