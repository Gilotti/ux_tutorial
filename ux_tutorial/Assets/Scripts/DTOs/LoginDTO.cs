﻿using UnityEngine;
using System.Collections;

public class LoginDTO : MonoBehaviour {

	string ClientId { get; set;}
	string ClientSecret { get; set;}
	string Provider { get; set;}
	string ProviderToken { get; set;}
	string Username { get; set;}
	string Password { get; set;}
	decimal Latitude { get; set;}
	decimal Longitude { get; set;}
	decimal Altitude { get; set;}

}
