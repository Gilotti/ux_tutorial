﻿using UnityEngine;
using System.Collections;

public class BeneficiariesAddDTO : MonoBehaviour {

	public string Name { get; set;}
	public string Relationship { get; set;}
	public string DateOFBirth { get; set;}
	public string Status { get; set;}
	public int Participation { get; set;}

}
