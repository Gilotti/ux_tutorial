﻿using UnityEngine;
using System.Collections;

public class LocationFromIpDTO : MonoBehaviour {

	public string Ip { get; set; }
	public string Country_code { get; set; }
	public string Country_name { get; set; }
	public string Region_code { get; set; }
	public string Region_name { get; set; }
	public string City { get; set; }
	public string Zip_code { get; set; }
	public string Time_zone { get; set; }
	public string Latitude { get; set; }
	public string Longitude { get; set; }
	public string Metro_code { get; set; }
}