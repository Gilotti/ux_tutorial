﻿using UnityEngine;
using System.Collections;

public class IdentityProvidersDTO : MonoBehaviour {

	public string LoginProvider { get; set; }
	public string LoginProviderKey { get; set; }
}
