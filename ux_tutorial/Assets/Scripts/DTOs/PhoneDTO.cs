﻿using UnityEngine;
using System.Collections;

public class PhoneDTO : MonoBehaviour {

	public string Id { get; set; }

	public string UserId { get; set; }
	public string Type { get; set; }
	public int CountryCode { get; set; }
	public int? StateCode { get; set; }
	public int CityCode { get; set; }
	public int PhoneNumber { get; set; }
	public string Complement { get; set; }
	public BiaTypesEnum.UserDataStatus Status { get; set; }
	public string CreatedBy_UserId { get; set; }
	public string DateCreated { get; set; }
	public string DateUpdated { get; set; }
}
