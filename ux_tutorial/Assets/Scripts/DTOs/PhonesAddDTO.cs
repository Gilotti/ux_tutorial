﻿using UnityEngine;
using System.Collections;

public class PhonesAddDTO : MonoBehaviour {

	string CountryCode { get; set;}
	string CityCode { get; set;}
	string PhoneNumber { get; set;}
	string UserPhoneType { get; set;}
	string StateCode { get; set;}
	string Complement { get; set;}

}
