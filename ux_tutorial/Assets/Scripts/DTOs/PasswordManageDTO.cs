﻿using UnityEngine;
using System.Collections;

public class PasswordManageDTO : MonoBehaviour {

	string CurrentPassword { get; set;}
	string NewPassword { get; set;}
	string ConfirmNewPassword { get; set;}

}
