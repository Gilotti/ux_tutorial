﻿using UnityEngine;
using System.Collections;

public class AddressAddDTO : MonoBehaviour {

	string Route { get; set;}
	string ZipCode { get; set;}
	string City { get; set;}
	string State { get; set;}
	string Country { get; set;}
	string Neighborhood { get; set;}
	string Number { get; set;}
	string Complement { get; set;}

}
