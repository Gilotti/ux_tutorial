﻿using UnityEngine;
using System.Collections;

public class AccountDetailsDTO : MonoBehaviour {

	string Name { get; set;}
	string UserName { get; set;}
	string Email { get; set;}
	string BirthDate { get; set;}
	string Gender { get; set;}
	string AvatarURI { get; set;}
	string LanguageCode { get; set;}
	string TimeZoneId { get; set;}

}
