﻿using UnityEngine;
using System.Collections;

public class UserBeneficiariesDTO : MonoBehaviour {

	public string Id { get; set; }
	public string ParentUserId { get; set; }
	public string Name { get; set; }
	public string Status { get; set; }
	public string Relationship { get; set; }
	public string DateOfBirth { get; set; }
	public int Participation { get; set; }
	public BiaDocumentsDTO Documents { get; set; }
	public string CreatedBy_UserId { get; set; }
	public string UpdatedBy_UserId { get; set; }
	public string DateCreated { get; set; }
	public string DateUpdated { get; set; }

}
