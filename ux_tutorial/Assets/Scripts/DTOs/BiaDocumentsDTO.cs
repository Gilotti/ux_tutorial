﻿using UnityEngine;
using System.Collections;

public class BiaDocumentsDTO : MonoBehaviour {

	public string Id { get; set; }
	public string ParentType { get; set; }
	public string ParentId { get; set; }
	public BiaTypesEnum.DocumentType DucumentType { get; set; }
	public string DocumentNumber { get; set; }
	public string ParentName { get; set; }
	public string EmittedDate { get; set; }
	public string Emitter { get; set; }
	public BiaTypesEnum.UserDataStatus Status { get; set; }
	public string CreatedBy_UserId { get; set; }
	public string DateCreated { get; set; }
	public string DateUpdated { get; set; }

} 
