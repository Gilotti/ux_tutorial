﻿using UnityEngine;
using System.Collections;

public class AccountCreateDTO : MonoBehaviour {

	string ClientId { get; set;}
	string ClientSecret { get; set;}
	string Provider { get; set;}
	string ProviderToken { get; set;}
	string Username { get; set;}
	string Email { get; set;}
	string Password { get; set;}
	string Gender { get; set;}
	string Birthday { get; set;}
	string LanguageCode { get; set;}
	string TimezoneId { get; set;}
	string AvatarURI { get; set;}

}
