﻿using UnityEngine;
using System.Collections;

public class UserLoginLogDTO : MonoBehaviour {

	public string ClientId { get; set; }
	public string ClientLanguage { get; set; }

	public string LoginDate { get; set; }
	public string IpAddress { get; set; }

	public decimal Latitude { get; set; }
	public decimal Longitude { get; set; }
}
