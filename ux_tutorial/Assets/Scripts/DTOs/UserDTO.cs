﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UserDTO : MonoBehaviour {

	public string Id { get; set; }
	public string Name { get; set; }
	public string UserName { get; set; }
	public string Email { get; set; }
	public bool EmailConfirmed { get; set; }
	public bool PhoneConfirmed { get; set; }
	public string BirthDate { get; set; }
	public BiaTypesEnum.GenderType gender { get; set; }
	public string LanguageCode { get; set; }
	public string TimeZoneId { get; set; }
	public string AvatarURI { get; set; }
	public BiaTypesEnum.UserDataStatus Status { get; set; }
	public string DateCreated { get; set; }
	public string DateUpdated { get; set; }
	public List<IdentityProvidersDTO> IdentityProviders { get; set; }
	public List<BiaDocumentsDTO> Documents { get; set; }
	public List<AddressDTO> Addresses { get; set; }
	public List<PhoneDTO> Phones { get; set; }
	public List<UserBeneficiariesDTO> Beneficiaries { get; set; }
	public List<UserLoginLogDTO> LastLogins { get; set; }
}
