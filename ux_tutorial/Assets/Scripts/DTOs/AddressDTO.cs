﻿using UnityEngine;
using System.Collections;

public class AddressDTO : MonoBehaviour {

	public string Id { get; set; }
	public string UserId { get; set; }
	public string Route { get; set; }
	public int? Number { get; set; }
	public string Complement { get; set; }
	public string City { get; set; }
	public string Neighborhood { get; set; }
	public string State { get; set; }
	public string Country { get; set; }
	public string ZipCode { get; set; }
	public BiaTypesEnum.UserDataStatus Status {get; set;}
	public string CreatedBy_UserId { get; set; }
	public string DateCreated { get; set; }
	public string DateUpdated { get; set; }
}
	
