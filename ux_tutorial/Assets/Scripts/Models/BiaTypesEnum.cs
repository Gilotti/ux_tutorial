﻿using UnityEngine;
using System.Collections;

public class BiaTypesEnum : MonoBehaviour {


	public enum UserDataStatus
	{
		None                        = 0,
		Active                      = 101,
		Inactive                    = 201,
		Analysis                    = 301,
		Aproved                     = 302,
		Denied                      = 303,
		Suspended                   = 402,
		Banned                      = 501,
		Archieved                   = 601,
		Deleted                     = 901,
	}



	public enum GenderType {
		None                        = 0,
		Female                      = 101,
		Male                        = 201,
		NotInformed                 = 301,
	}



	public enum PhoneType
	{
		None                        = 0,
		Residential                 = 101,
		Work                        = 102,
		Mobile                      = 103,
		Friend                      = 104,
		Parent                      = 105,

	}



	public enum DocumentType
	{
		None                        = 0,
		Identity                    = 101,
		CPF                         = 102,
		Passport                    = 103,
		SocialNumber                = 104,
		SUSEP                       = 105,
		CNPJ                        = 106,
	}



	public enum RelationshipType
	{
		None                        = 0,
		Father                      = 101,
		Mother                      = 102,
		Son                         = 103,
		Daughter                    = 104,
		Brother                     = 105,
		Syster                      = 106,
		Uncle                       = 201,
		Aunt                        = 202,
		Cousin                      = 203,
		Nephew                      = 204,
		Grandfather                 = 301,
		Grandmother                 = 302,
		Grandchild                  = 303,
		FatherInLaw                 = 401,
		BrotherInLaw                = 402,
		DaughterInLaw               = 403,
		SonInLaw                    = 404,
		Husband                     = 501,
		Wife                        = 502,
		Friend                      = 601,
		Boyfriend                   = 602,
		Girlfriend                  = 603,
	}

}
